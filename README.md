<b>Robot City Navigation System</b>
<br>
Routes are stored in db in table serve_route
Landmarks are stored in serve_landmark
See explanation of columns in model.py

New routes can be loaded through file upload UI. Form is protected with csfr token
http://localhost:8000/serve 
See example of file in example_route.txt

  Example:
1. Start (x, y)
2. Go North 5 blocks #blocks is optional. There can be any text after number 
3. Turn right #Change direction, see route_parser.turn_enum for possible values
4. Go until "Statue of Old Man with Large Hat" #This landmark has to be registered in table in advance
5. Go 3 blocks #Keep previous direction, walk ahead

Parser is skipping empty lines and is able to ingest many routes in one file.
Each route starts with start command.

Robots can access data through 2 APIs:
http://localhost:8000/serve/getpath?fromx=245&fromy=161&tox=220&toy=166
Returns list of coordinates on a path

http://localhost:8000/serve/getpath?fromx=245&fromy=161&tox=220&toy=166
Returns directions and coords

Programmers can expose as many endpoints as needed to fulfill any robots commands requirements.

Path is extracted in path_resolver._get_path method.
Program will be able to fetch path if previously there was a request from x1,y1 to x2,y2, even if these dots were somewhere in a middle of path.
At the moment system is not able to fetch reverse paths. Also system is not merging different paths together.

    Poptential improvements:
1. System with million of routes requires indexing of Route table (example done in models.py). If table is too big, sharding is possible
2. If amount of instructions is very big, first need to consider merging different routes together. Application can store nodes of graph instead ov vertices of graph (meaning Route entity will have fromx, fromy, tox, toy).
File ingestion can happen in parallel manner
3. Millions of users (robots) will require palatalization of path resolving. This can be achieved with celery queue. Also we can start several nodes in docker, with load balancer in front of them. 
Good performance improvement can be achieved by adding caching in application, both in django (lru_cache) and some external caching system (e.g. memcached, redis, etc)
4. Heavy changes of db data with indexes impacts performance significantly, as index has to be recalculated on each update. First, need to consider sharding, secondly - archiving strategies. The idea is to make recalculation of indexes as quick as possible by lowering amount of indexed data.