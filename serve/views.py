from django.http import HttpResponse
from django.shortcuts import render

from .forms import UploadRouteForm
from .services.path_resolver import _get_path
from .services.route_parser import parse_file
from .services.route_util import store_route
from django.db import transaction

@transaction.atomic
def upload_file(request):
    """
    Process file with routes upload
    """
    if request.method == 'POST':
        form = UploadRouteForm(request.POST, request.FILES)
        if form.is_valid():
            routes = parse_file(request.FILES['file'])
            for route in routes:
                store_route(route)
            return render(request, 'upload.html', {
                'success': "True"
            })
    else:
        form = UploadRouteForm()
    return render(request, 'upload.html', {'form': form})

def get_path(request):
    """
    Get path by coordinates
    """
    fromx, fromy, tox, toy = request.GET['fromx'], request.GET['fromy'], request.GET['tox'], request.GET['toy'],
    res = _get_path(fromx, fromy, tox,toy)
    if res is None:
        return HttpResponse("No path stored in db")
    return HttpResponse(res)

def extract_direction(x, y, x2, y2):
    if x2 > x: return 'east'
    if x2 < x: return 'west'
    if y2 > y: return 'north'
    if y2 < y: return 'east'
    return None

def get_instructions_path(request):
    """
    Get instructions list in human-readable format
    """
    fromx, fromy, tox, toy = request.GET['fromx'], request.GET['fromy'], request.GET['tox'], request.GET['toy'],
    path = _get_path(fromx, fromy, tox, toy)
    if path is not None:
        step = None
        route = []
        for r in path:
            if step is None:
                step = (None, *r)
            else:
                step = (extract_direction(step[1], step[2], *r), *r)
            route.append(step)
        return HttpResponse(route)
    return HttpResponse("No path stored in db")
