from ..models import Route

def _get_path(fromx, fromy, tox,toy):
    """
    Fetch path between dots
    :param fromx: start - X coordinate
    :param fromy: start - Y coordinate
    :param tox: end - X coordinate
    :param toy: end - Y coordinate
    :return:
    """
    rs = Route.objects.filter(x=fromx, y=fromy, step=0)
    for r in rs:
        rs2 = Route.objects.filter(x=tox, y=toy, step__gt=r.step, routeId=r.routeId)
        if len(rs2):
            r2 = rs2[0]
            return [(r.x, r.y) for r in Route.objects.filter(routeId=r2.routeId, step__range=(r.step, r2.step))]
    return None