from ..models import Landmark
import logging
from collections import OrderedDict as odict

"""
module, intended to parse uploaded file and store in db
In db route is stored as list of coords for each step
This is useful as different robots can require different format of commands, so it makes sense to store raw data.
For each particular robot we will eb able to provide information in custom format
"""

#All possible actions
ACTIONS = ['start', 'go', 'turn']
#All possible directions
DIRECTIONS = odict([('west', (-1, 0)), ('north', (0, 1)), ('east', (1, 0)), ('south', (0, -1))])
#All possible turns
TURNS = {'left', 'right'}

def turn_direction(direction, turn):
    """
    Change robot direction
    :param direction: DIRECTIONS item
    :param turn: TURNS item
    :return: new direction
    """
    i = list(DIRECTIONS.values()).index(tuple(direction))
    if turn == 'left':
        i -= 1
    else:
        i = (i + 1) % len(DIRECTIONS)
    return list(DIRECTIONS.values())[i]

def parse_file(f):
    """
    Parse input file with routes
    :param f: file
    :return: ist of parsed routes
    """
    start = None
    direction = None
    routes =[]
    route = []
    failedRoute = False #Turns into True when route contains error. Skip all steps until next 'start'
    for l in f:
        if len(l): #Skip empty lines
            l = l.decode('utf-8').lower().strip() #clear string
            try:
                steps = 0
                end = None
                ss = l.split(' ') #Turn str into set of commands
                if ss[0] == 'start': #If start - end current route and start new
                    if len(route):
                        routes.append(route)
                        route = []
                    end = [int(i) for i in ' '.join(ss[1:]).replace('(','').replace(')','').split(',')]
                    failedRoute = False
                if not failedRoute: #If current route has faield step - skip the rest
                    if ss[0] == 'turn': #If command is turn - change robot direction and don't move
                        direction = turn_direction(direction, ss[1])
                        end = start
                    elif ss[0] == 'go': #If command go - check second command
                        if ss[1] == 'until': #go until meet landmark. Landmark has to be registered in db
                            landmark_name = ' '.join(ss[2:]).replace('"','').replace("'",'')
                            landmark = Landmark.objects.filter(name=landmark_name)
                            if not len(landmark):
                                raise Exception("No stored Landmark {}".format(landmark_name))
                            landmark = landmark[0]
                            end = (landmark.x, landmark.y)
                        elif ss[1] in DIRECTIONS: #go west
                            direction = DIRECTIONS[ss[1]]
                            steps = int(ss[2])
                        elif ss[1] in TURNS: #go left
                            direction = turn_direction(direction, ss[1])
                            steps = int(ss[2])
                        else:
                            steps = int(ss[1]) #keep previous direction, move N steps
                    if end is None:
                        end = [route[-1][0] + direction[0] * steps, route[-1][1] + direction[1] * steps] #calculate next coords
                    if len(route) == 0 or route[-1] != end:
                        route.append(end) #collect route
            except Exception as e:
                logging.error('Failed to parse route ', e)
                failedRoute = True
                route = []
    if len(route):
        routes.append(route)
    return routes


