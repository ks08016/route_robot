import uuid
from ..models import Route

def store_route(routes):
    """
    Save route
    """
    if len(routes):
        id = str(uuid.uuid1())
        for route in routes:
            for i,r in enumerate(route):
                Route(routeId=id, step=i, x=r[0], y=r[1]).save()