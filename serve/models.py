from django.db import models
from django.dispatch import Signal

pre_update = Signal()

class MyCustomQuerySet(models.query.QuerySet):
    def update(self, **kwargs):
        if 'f' not in kwargs:
            kwargs['f'] = 0
        super(MyCustomQuerySet, self).update(**kwargs)

class MyCustomManager(models.Manager):
    def get_queryset(self):
        return MyCustomQuerySet(self.model, using=self._db)


"""
Storage for landmarks in city
"""
class Landmark(models.Model):
    name = models.CharField(max_length=300) #Name of landmark
    x = models.IntegerField() #Landmark coords
    y = models.IntegerField()

"""
Storage for route 
"""
class Route(models.Model):
    routeId = models.TextField(db_index=True) #route identifier
    step = models.IntegerField(db_index=True) #step number
    x = models.IntegerField() #step.x
    y = models.IntegerField() #step.y
    f = models.IntegerField(default=0) # flag

    objects = MyCustomManager()


