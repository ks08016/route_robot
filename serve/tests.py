from django.test import TestCase

from .services.route_parser import parse_file
from .services.route_util import store_route
from .services.path_resolver import _get_path
from .models import Route

class TestParseFile(TestCase):
    def test_file_parse(self):
        d = ['Start (245, 161)','Go North 5 blocks','Turn right']
        assert len(parse_file([bytes(a, "utf-8") for a in d])) == 1
        d = ['Start (245, 161)', 'Go Northx 5 blocks', 'Turn right']
        assert len(parse_file([bytes(a, "utf-8") for a in d])) == 0
        assert len(parse_file([])) == 0

    def test_two_routes(self):
        d = ['Start (245, 161)', 'Go North 5 blocks', 'Turn right', 'Start (245, 161)', 'Turn left']
        assert len(parse_file([bytes(a, "utf-8") for a in d])) == 2

class TestRoute(TestCase):
    def setUp(self):
        Route.objects.all().delete()
        d = ['Start (245, 161)', 'Go North 5 blocks', 'Turn right']
        store_route(parse_file([bytes(a, "utf-8") for a in d]))
        assert Route.objects.count() == 2

    def test_route(self):
        path =_get_path(245, 161, 245, 166)
        assert len(path) == 2
        assert path[0] == (245,161)
        assert _get_path(0,0,1,1) is None

    def test_overlapping_route(self):
        d = ['Start (245, 161)', 'Go South 5 blocks', 'Turn right']
        store_route(parse_file([bytes(a, "utf-8") for a in d]))
        assert Route.objects.count() == 4
        path = _get_path(245, 161, 245, 166)
        assert len(path) == 2

class TestSignal(TestCase):
    def test_rewrite(self):
        Route.objects.create(routeId='1', step=1, x=1, y=2)
        Route.objects.filter(routeId=1).update(x=2)
        Route.objects.filter(routeId=1).update(x=3, f=3)
        Route.objects.filter(routeId=1).update(x=4)